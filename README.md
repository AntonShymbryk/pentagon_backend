# README #


## 1) Authorization page ##

* mapping: /auth
* http POST-request
* format JSON
* Validation:

      String email = user's email (not empty, not null);

      String password = user's password (not empty, not null, first capital letter, 8-16 symbols, numbers required, special characters forbidden);



***
If authorization is successful
- 
>#### 1) Status: OK

- #### Request body:

{ "email": "lily@gmail.com", "password": "Qwerty123" }

 - #### Response body: 

{"code": 200, "type": "unknown", "message": "logged in user session:1643304005288"}

***

If authorization is unsuccessful
-

>#### 1) Status: 404 Not Found


- #### Request body:

{"email": "shura@mail.ru", "password": "Shura123"}

- #### Response body:

{"code": 1, "type": "error", "message": "User not found"}

***
>#### 2) Status: 400 Bad Request

- #### Request body:

{"email": "lily.mail.ru", "password": "Qwerty123"}

- #### Response body:

{"code": 1, "type": "error", "message": "Email is invalid"}

***

>#### 3) Status: 400 Bad Request

- #### Request body:

{"email": "lily@gmail.com", "password": "кверти123"}

- #### Response body:

{"code": 1, "type": "error", "message": "Password is invalid"}

***

>#### 4) Status: 401 Unauthorized

- #### Request body:

{"email": "lily@gmail.com", "password": "qwerty123"}

- #### Response body:

{"code": 1, "type": "error", "message": "Password is invalid"}

***
>#### 5) Status: 403 Forbidden

- #### Request body:

{"email": "spam@gmail.com", "password": "SPAM1000"}

- #### Response body:

{"code": 1, "type": "error", "message": "Access is forbidden"}

***
>#### 6) Status: 400 Bad Request

- #### Request body:

{"email": "", "password": "Qwerty123"}

- #### Response body:

{"code": 1, "type": "error", "message": "Email is empty"}

***
>#### 7) Status: 400 Bad Request

- #### Request body:

{"email": "lily@gmail.com", "password": ""}

- #### Response body:

{"code": 1, "type": "error", "message": "Password is empty"}

***

>#### 8) Status: 400 Bad Request

- #### Request body:

{"email": "", "password": ""}

- #### Response body:

{"code": 1, "type": "error", "message": ""Required fields are empty""}


***

## 2) Registration page ##
* mapping: /reg
* http POST-request
* format JSON
* Validation:


     String login = user's login (not empty, not null, latin letters, numbers and special characters allowed);

     String email = user's email (not empty, not null);

     String password = user's password (not empty, not null, first capital letter, 8-16 symbols, numbers required, special characters forbidden);

     String passwordConfirm = password;

     String phoneNumber = user's number (not empty, not null, 11-12 numbers, special characters allowed);

     String companyName = "" (optional);


***

If registration successful
-

>#### 1) Status: 201 Created

- #### Request body:  

{"login": "lily", "email": "lily93@gmail.com", "password": "Qwerty1234", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}

- #### Response body: ###

{"code": 201, "type": "unknown", "message": "User is created"}

***

If registration unsuccessful
-

>#### 1) Status: 400 Bad Request

- #### Request body:

{"login": "lyly", "email": "lily93@gmail.com", "password": "Qwerty1234", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Invalid login supplied"}

***
>#### 2) Status: 400 Bad Request

- #### Request body:

{"login": "lily", "email": "lily93@mail.com", "password": "Qwerty1234", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Invalid email supplied"}

***

>#### 3) Status: 400 Bad Request

- #### Request body:

{"login": "lily", "email": "lily93@mail.com", "password": "qwerty1234", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Invalid password supplied"}

***

>#### 4) Status: 400 Bad Request

- #### Request body:

{"login": "lily", "email": "lily93@mail.com", "password": "Йцуке123", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Passwords don't match"}

***

>#### 5) Status: 400 Bad Request

- #### Request body:

{"login": "lily", "email": "lily93@mail.com", "password": "Йцуке123", "passwordConfirm": "Qwerty1234", "phone": "+38-(098)6754673", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Invalid phone number supplied"}

***

>#### 6) Status: 400 Bad Request

- #### Request body:

{"login": "", "email": "", "password": "", "passwordConfirm": "", "phone": "", "company": ""}

- #### Response body: ###

{"code": 1, "type": "error", "message": "Required fields are empty"}

***

>#### 7) Status: 409 Conflict

- #### Request body:

{"login": "lily", "email": "lily93@gmail.com", "password": "Qwerty1234", "passwordConfirm": "Qwerty1234", "phone": "+38(098)6754673", "company": ""}


- #### Response body: ###

{"code": 1, "type": "error", "message": "User with such data is already exists"}

***

Database schema
-

![alt text](https://fv2-3.failiem.lv/thumb_show.php?i=2jv3m9j9q&view)
